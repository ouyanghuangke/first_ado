﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string ConnectionString = "Server=.;Database=MyCar;UId=sa;Pwd=123456;";

            SqlConnection connection = new SqlConnection(ConnectionString);

            connection.Open();

            Console.WriteLine("打开成功，状态" + connection.State);

            string sqlselect = "select * from Car ";

            SqlCommand cmd = new SqlCommand(sqlselect, connection);

            SqlDataReader sdr = cmd.ExecuteReader();

            while (sdr.Read())
            {
                Console.WriteLine("编号：" + sdr["Id"] + "，车名：" + sdr["Title"] + "，速度：" + sdr["Speed"]);

            }
            connection.Close();
            Console.WriteLine("关闭数据库成功" + connection.State);
            Console.Read();

        }
    }
}
