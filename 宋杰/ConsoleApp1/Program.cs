﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = "server=.;database=Test;uid=sa;pwd=123456";

            SqlConnection sqlConnection = new SqlConnection(connectionString);

            sqlConnection.Open();



            var sqlCommandSting = "select * from Username";

            SqlCommand sqlCommand = new SqlCommand(sqlCommandSting, sqlConnection);

            var sqlDataReader = sqlCommand.ExecuteReader();

            while (sqlDataReader.Read())
            {
                Console.WriteLine("编号：" + sqlDataReader["Id"] + "，用户名：" + sqlDataReader["Name"] + "性别：" + sqlDataReader["Sex"]);
            }

            sqlConnection.Close();

            Console.Read();
        }
    }
}
