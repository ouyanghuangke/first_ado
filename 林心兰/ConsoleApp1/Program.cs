﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString= "server=LAPTOP-MCU8SB1M;database=school;uid=sa;pwd=123456789";

            SqlConnection sqlConnection = new SqlConnection(connectionString);

            sqlConnection.Open();

            var sqlCommandString= "select * from userinfo";

            SqlCommand sqlCommand = new SqlCommand(sqlCommandString,sqlConnection);

            var sdr = sqlCommand.ExecuteReader();

            while(sdr.Read())
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}",sdr["userid"],sdr["username"], sdr["userpwd"], sdr["userage"],sdr["usersex"]);
            }

            sqlConnection.Close();
        }
    }
}
