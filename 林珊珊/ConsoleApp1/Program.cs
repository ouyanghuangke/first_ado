﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            //连接字符串
            string connStr = "server=DESKTOP-512404B;database=Test;uid=sa;pwd=123456";
            //声明对象
            SqlConnection conn = new SqlConnection(connStr);

            conn.Open();

            string sql = "select * from Users";
            SqlCommand cmd = new SqlCommand(sql, conn);

            SqlDataReader sdr = cmd.ExecuteReader();

            while (sdr.Read()) 
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}\t", sdr["id"], sdr["name"], sdr["sex"], sdr["age"]);
            }
            conn.Close();
        }
    }
}
