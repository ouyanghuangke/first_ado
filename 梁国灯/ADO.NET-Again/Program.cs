﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADO.NET_Again
{
    class Program
    {
        static void Main(string[] args)
        {
            //连接字符串
            string connStr = "server = . ; database = dbdemo ; uid = sa ; pwd = 193746";
            //声明连接对象
            SqlConnection conn = new SqlConnection(connStr);
            //打开连接
            conn.Open();
            //Sql查询语句
            string query = "SELECT * FROM userInfo";
            //声明适配器对象
            SqlDataAdapter sda = new SqlDataAdapter(query, conn);
            //声明数据表对象
            DataTable dt = new DataTable();
            //将查询到的数据填充至dt中
            sda.Fill(dt);
            //断开数据连接
            conn.Close();
            //执行查询语句
            Console.WriteLine("姓名\t密码\t年龄\t性别"); //标记行
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                DataRow dr = dt.Rows[i]; //提取第i行的所有数据
                //将每一列数据单独放入一个变量中
                string userName = (string)dr["userName"];
                string userPwd = (string)dr["userPwd"];
                int userAge = (int)dr["userAge"];
                string userSex = (string)dr["userSex"];
                Console.WriteLine(userName + "\t" + userPwd + "\t" + userAge + "\t" + userSex);
            }
            Console.ReadLine();

        }
    }
}
