﻿using System;
using System.Data.SqlClient;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = "server=.;database=lsq;uid=sa;pwd=123456";
            SqlConnection sqlConnection = new SqlConnection(connectionString);
            sqlConnection.Open();
            var sqlCommandString = "select * from Users";
            SqlCommand sqlCommand = new SqlCommand(sqlCommandString, sqlConnection);
            var sdr = sqlCommand.ExecuteReader();
            while (sdr.Read())
            {
                Console.WriteLine("{0}\t{1}\t{2}\t{3}", sdr["Id"], sdr["Name"], sdr["Age"]);
            }
            sqlConnection.Close();

        }
    }
}
